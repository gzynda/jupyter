Image: taccsciapps/jupyteruser-base

Description: The base jupyter notebook image for taccsci

Creates:
 - /etc/jupyteruser-base-release --  Access version from inside container

Kernels:
 - Bash
 - Python2
 - Python3
 - R

Version: 0.1.2
