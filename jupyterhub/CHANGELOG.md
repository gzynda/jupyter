# Change Log
All notable changes to this project will be documented in this file.

## alpha-1.2.0 - 2017-05-26
### Added
- Add support for running the Jupyter notebook as the user's uid and gid as returned by the TAS API.
- Add support for tas_homeDirectory, for mounting TACC
- Make check for the existence of a mydata directory using the agavepy files service optional.
- Make the token cache files world writeable so that refresh can work when the notebook is running as an arbitrary UID.

### Changed
- Modified paths related to JupyterHub/dockerspawner internals to use `self.user.name` instead of `self.escaped_name`.
This resolves an issue where

### Removed
- No change.


## alpha-1.1.1 - 2017-06-25
### Added
- No change.

### Changed
- Fixed bug in spawner (line 356) where an invalid log statement would cause an exception.

### Removed
- No change.


## alpha-1.1.0 - 2017-05-26
### Added
- Add optional and configurable project mounts feature for mounting project directories based on a projects API call.
- Check for the existence of a mydata directory using the agavepy files service.

### Changed
- No change.

### Removed
- No change.


## alpha-1.0.0 - 2017-03-07
### Added
- No change.

### Changed
- (#30) Fix bug in files.getHistory operation where systemId parameter was missing.

### Removed
- No change.


