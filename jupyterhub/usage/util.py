import datetime
import os
import sqlite3
import statistics

db_file = os.environ.get('db_file', '/data/resources.db')

def get_cursor():
    conn = sqlite3.connect(db_file)
    return conn.cursor()

def execut_sql(sql, cursor=None):
    if not cursor:
        cursor = get_cursor()
    cursor.execute(sql)
    return cursor.fetchall()

def get_records_by_date_range(start, end):
    return execut_sql("select * from resources where date < '{}' and date > '{}' ;".format(end, start))

def get_unique_users_by_date_range(start, end):
    results = get_records_by_date_range(start, end)
    return {r[4] for r in results}

def get_all_user_counts(start='2017-12-05', end='2018-01-05'):
    d1 = datetime.datetime.strptime(start, '%Y-%m-%d')
    d2 = datetime.datetime.strptime(end, '%Y-%m-%d')
    d = d1
    step = datetime.timedelta(days=1)
    results = []
    while d + step < d2:
        results.append(get_unique_users_by_date_range(d.date(), (d+step).date()))
        d += step
    return results


# some examples:
results = get_all_user_counts(start='2017-12-04', end='2018-03-01')
d1 = datetime.datetime.strptime('2017-12-04', '%Y-%m-%d')
d2 = datetime.datetime.strptime('2018-03-01', '%Y-%m-%d')
d = d1
step = datetime.timedelta(days=1)
i = 0
while d + step < d2:
    print(d.date(), len(results[i]))
    d += step
    i += 1

statistics.mean([len(results[i]) for i in range(len(results))])
