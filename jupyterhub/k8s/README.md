# TACC JupyterHub Running on Kubernetes #

Experimental Docker image to get the custom TACC JupyterHub running on our internal k8s cluster.

Build the image:
```
$ docker build -t taccsciapps/jupyterhub:k8s .
```

Create the deployment and service:
```
$ kubctl create -f jhub_deployment.yml
$ kubctl create -f jhub_service.yml
```

Be sure the default service account has permissions to create pods via the k8s api. For a quick fix:
```
$ kubectl create rolebinding wideOpenRoleBinding --clusterrole=admin --serviceaccount=default:default --namespace=default
```

